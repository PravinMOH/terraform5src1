variable "public_ssh_key" {
    description = "public ssh key for ec2 instance"
    type = string
  
}
variable "ec2_ami" {
    description = "ID of AMI for ec2 instance"
    type = string
}

variable "key_name" {
    description = "Name of public ssh key for ec2 instance"
    type = string
}
variable "instance_type" {
    description = "type of instance ec2"
    type = string
}
variable "count_instances" {
    description = "number of instance to create"
    type = number
  
}
variable "vpc_name" {
    description = "name of vpc"
    type = string
}
variable "aws_instance_name" {
    description = "name of aws instance"
    type = string
}
variable "security_group_name" {
    description = "name of security group"
    type = string
}
variable "ec2_name" {
    description = "name of ec2"
    type = string
}
variable "from_port_in" {
    description = "from_port_in"
    type = number
}
variable "to_port_in" {
    description = "to_port_in"
    type = number
}
variable "protocol_in" {
    description = "protocol_in"
    type = string
}
variable "from_port_ext" {
    description = "from_port_ext"
    type = number
}
variable "to_port_ext" {
    description = "to_port_ext"
    type = number
}
variable "protocol_ext" {
    description = "protocol_ext"
    type = number
}
