#Creation d'une instance EC2
resource "aws_instance" "Pravin" {
  count = "${var.count_instances}"
  ami = "${var.ec2_ami}"
  instance_type = "${var.instance_type}"
  subnet_id = aws_subnet.test_subnet.id
  vpc_security_group_ids = [ aws_security_group.allow-ssh.id ]
  associate_public_ip_address = true
  key_name = aws_key_pair.deployer.key_name
  tags = {
    Name = "${var.ec2_name}"
    }
}
resource "aws_key_pair" "deployer" {
  key_name   = "${var.key_name}"
  public_key = "${var.public_ssh_key}"
}
#Creation du Bucket S3
resource "aws_s3_bucket" "Pravin-bucket" {
  bucket = "pravin17-bucket"
  tags = {
    Name        = "bucket"
    Environment = "Dev"
  }
}

resource "aws_s3_bucket_policy" "bucket-policy" {
  bucket = aws_s3_bucket.Pravin-bucket.id
  policy = <<POLICY
  {
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "AllowOnIPAddress",
      "Effect": "Deny",
      "Principal": "*",
      "Action": "s3:*",
      "Resource": [
        "arn:aws:s3:::pravin17-bucket/*"
      ],
      "Condition": {
         "NotIpAddress": {"aws:SourceIp": "54.145.18.151/32"}
      }
    }
  ]
}
POLICY
}
resource "aws_s3_bucket_acl" "Pravin-bucket" {
  bucket = aws_s3_bucket.Pravin-bucket.id
  acl    = "private"
}

#Creation du VPC TEST
resource "aws_vpc" "Pravin-VPC" {
  cidr_block       = "10.10.0.0/16"
  instance_tenancy = "default"

  tags= {
    Name= "${var.vpc_name}"
  }
}
#creation reseau
resource "aws_subnet" "Pravin-SUBNET" {
  vpc_id            = aws_vpc.vpc_test.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "us-east-1e" # zone disponible a

  tags= {
    Name= "subnet_test"
  }
}

#creation gateway
resource "aws_internet_gateway" "gw_test" {
    vpc_id = aws_vpc.vpc_test.id

  tags= {
    Name= "gateway_test"
  }
}

#les routes
resource "aws_route_table" "publique_route_test" {
    vpc_id = aws_vpc.vpc_test.id
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.gw_test.id
    }
    
}

resource "aws_route_table_association" "test" {
    subnet_id      = aws_subnet.test_subnet.id
    route_table_id = aws_route_table.publique_route_test.id
}
#Creation du security group
resource "aws_security_group" "allow-ssh" {
  name        = "${var.security_group_name}"
  description = "Allow ssh outbound and inbound traffic"
  vpc_id      = aws_vpc.vpc_test.id

  ingress {
    description      = "allow ssh inbound"
    cidr_blocks = ["0.0.0.0/0"]
    from_port        = "${var.from_port_in}"
    to_port          = "${var.to_port_in}"
    protocol         = "${var.protocol_in}"
  
  }

  egress {
    description      = "allow allow outbound" 
    cidr_blocks = ["0.0.0.0/0"]
    from_port        = "${var.from_port_ext}"
    to_port          = "${var.to_port_ext}"
    protocol         = "${var.protocol_ext}"
  
  }

  tags = {
    Name = "allow_ssh"
  }
}
