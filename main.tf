provider "aws" {
  # Configuration options
  region     = var.region_instance
  access_key = var.access_key_instance
  secret_key = var.secret_key_instance
  token= var.token_key_instance
}

module "demo_instance" {
    source = "./modules"
    key_name = var.key_name
    count_instances = var.count_instances
    vpc_name = var.vpc_name
    aws_instance_name = var.aws_instance_name
    public_ssh_key = var.public_ssh_key
    ec2_ami = var.ec2_ami
    instance_type = var.instance_type
    security_group_name = var.security_group_name
    ec2_name = var.ec2_name
    from_port_in =var.from_port_in
    to_port_in  = var.to_port_in
    protocol_in = var.protocol_in
    from_port_ext =var.from_port_ext
    to_port_ext  = var.to_port_ext
    protocol_ext = var.protocol_ext
}